import pandas as pd
import re  
import nltk
import numpy as np
nltk.download('stopwords')
nltk.download('punkt')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.snowball import SnowballStemmer

from sklearn.cluster import AgglomerativeClustering 




excel_data_df = pd.read_excel('DATA_SET_DESCRIMINACION.xlsx', sheet_name='Hoja 1') ## Hacer diccionario tokenizado





clase1=np.array(excel_data_df["Insultos Sexuales (clase 0)"])
clase2=np.array(excel_data_df["Insultos Discriminatorios (clase 1)"])
clase3=np.array(excel_data_df["Insultos Xenofobicos"])
clase4=np.array(excel_data_df["Insultos por casta (clase 3)"])
clase5=np.array(excel_data_df["Insultos internacionales (clase 4)"])
clase6=np.array(excel_data_df["No insultos Clase 5"])

clase1=" ".join(clase1[0:505])
clase2=" ".join(clase2[0:505])
clase3=" ".join(clase3[0:499])
clase4=" ".join(clase4[0:505])
clase5=" ".join(clase5[0:505])
clase6=" ".join(clase6[0:5299])


collection = [clase1,clase2,clase3,clase4,clase5,clase6]

array = []
for i,doc in enumerate(collection):
  array.append(re.sub('[^A-Za-z\u00C0-\u017F]+', ' ',collection[i].lower())) 


tokens = []   
for i in range(len(array)):
  tokens.append(word_tokenize(array[i]))

final_tokens=[]
sw = stopwords.words("spanish")
for i in range(len(tokens)):
  final_tokens.append([w for w in tokens[i] if not w.lower() in sw])

dic = final_tokens # diccionario a cargar  en database 

print(dic)