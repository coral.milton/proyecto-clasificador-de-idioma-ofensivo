import pandas as pd
import re  
import nltk
import numpy as np
nltk.download('stopwords')
nltk.download('punkt')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.neural_network import MLPClassifier
import pickle



excel_data_df = pd.read_excel('DATA_SET_DESCRIMINACION.xlsx', sheet_name='Hoja 1')




clase1=np.array(excel_data_df["Insultos Sexuales (clase 0)"])
clase2=np.array(excel_data_df["Insultos Discriminatorios (clase 1)"])
clase3=np.array(excel_data_df["Insultos Xenofobicos"])
clase4=np.array(excel_data_df["Insultos por casta (clase 3)"])
clase5=np.array(excel_data_df["Insultos internacionales (clase 4)"])
clase6=np.array(excel_data_df["No insultos Clase 5"])


clase1=" ".join(clase1[0:505])
clase2=" ".join(clase2[0:505])
clase3=" ".join(clase3[0:499])
clase4=" ".join(clase4[0:505])
clase5=" ".join(clase5[0:505])
clase6=" ".join(clase6[0:5299])

collection = [clase1,clase2,clase3,clase4,clase5,clase6]

array = []
for i,doc in enumerate(collection):
  array.append(re.sub('[^A-Za-z\u00C0-\u017F]+', ' ',collection[i].lower())) 


tokens = []   
for i in range(len(array)):
  tokens.append(word_tokenize(array[i]))

final_tokens=[]
sw = stopwords.words("spanish")
for i in range(len(tokens)):
  final_tokens.append([w for w in tokens[i] if not w.lower() in sw])

dic = final_tokens




vectorizer = TfidfVectorizer()  # Td_idf funcion 

X = vectorizer.fit_transform([" ".join(dic[0])," ".join(dic[1])," ".join(dic[2])," ".join(dic[3])," ".join(dic[4])," ".join(dic[5])]) #obtener tf_idf


data=X.toarray()  #datos
y=np.matrix("0;1;2;3;4;5") 





clf = MLPClassifier(solver='adam', alpha=1e-5,hidden_layer_sizes=(100,100), random_state=1)

clf.fit(data, y)

pickle.dump(clf, open("modelo2", 'wb'))
pickle.dump(vectorizer, open("tf_idf", 'wb')) # guardar tf_idf