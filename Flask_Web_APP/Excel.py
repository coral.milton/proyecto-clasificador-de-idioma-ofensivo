########################Analisis y prueba modelo final
import pandas as pd
import re  
import nltk
import numpy as np
nltk.download('stopwords')
nltk.download('punkt')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import pickle

loaded_model = pickle.load(open("modelo1", 'rb'))  # abrir modelo 
loaded_tf_idf=pickle.load(open("tf_idf", 'rb'))

excel_data_df = pd.read_excel('data set analisis de sentimientos twitter.xlsx', sheet_name='test data set')
excel_data_df=pd.DataFrame(excel_data_df).dropna(axis=1, how='all')
excel_data_df["Column1"]=excel_data_df["Column1"].str.replace('[^A-Za-z\u00C0-\u017F]+', ' ')
excel_data_df["Column1"]=excel_data_df["Column1"].str.lower()
D1=np.array(excel_data_df["Column1"])

pattern = re.compile(r'\b(' + r'|'.join(stopwords.words('spanish')) + r')\b\s*')


contador=0
aux=[]
while contador<len(D1):
  X_test= loaded_tf_idf.transform([pattern.sub('', str(D1[contador]))]).toarray()
  pred=loaded_model.predict(X_test)
  aux.append(pred[0])
  contador=contador+1


archivo_dudoso=pd.DataFrame(np.column_stack((D1,aux)))
archivo_dudoso.to_excel(r'Clasificacion Dudosa.xlsx', sheet_name='Hoja1', index=False)