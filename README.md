# Offensive Language Classifier

This project is a web application that classifies text as offensive or not, using Natural Language Processing (NLP) techniques and machine learning algorithms such as Naive Bayes, neural networks, and distance matrices. The app is hosted using Flask, providing an interactive interface for users to submit text and receive a classification.

## Features

- **Text Classification**: Classifies input text into offensive or non-offensive categories.
- **Machine Learning**: Uses multiple machine learning techniques, including:
  - Naive Bayes
  - Neural Networks
  - Distance Matrices
- **Flask Web Application**: The app is hosted using Flask and provides a simple web interface for interacting with the classifier.

## Technologies Used

- **Flask**: Python web framework for hosting the application.
- **Natural Language Processing (NLP)**: Used for text preprocessing and feature extraction.
- **Machine Learning Algorithms**:
  - Naive Bayes
  - Neural Networks
  - Distance Matrices


