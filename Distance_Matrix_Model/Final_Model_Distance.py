import pandas as pd
import re  
import nltk
import numpy as np
nltk.download('stopwords')
nltk.download('punkt')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.snowball import SnowballStemmer
import math
from sklearn.cluster import AgglomerativeClustering 
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfVectorizer
import textdistance


excel_data_df = pd.read_excel('DATA_SET_DESCRIMINACION.xlsx', sheet_name='Hoja 1')




clase1=np.array(excel_data_df["Insultos Sexuales (clase 0)"])
clase2=np.array(excel_data_df["Insultos Discriminatorios (clase 1)"])
clase3=np.array(excel_data_df["Insultos Xenofobicos"])
clase4=np.array(excel_data_df["Insultos por casta (clase 3)"])
clase5=np.array(excel_data_df["Insultos internacionales (clase 4)"])
clase6=np.array(excel_data_df["No insultos Clase 5"])

clase1=" ".join(clase1[0:505])
clase2=" ".join(clase2[0:505])
clase3=" ".join(clase3[0:499])
clase4=" ".join(clase4[0:505])
clase5=" ".join(clase5[0:505])
clase6=" ".join(clase6[0:5299])

collection = [clase1,clase2,clase3,clase4,clase5,clase6]

array = []
for i,doc in enumerate(collection):
  array.append(re.sub('[^A-Za-z\u00C0-\u017F]+', ' ',collection[i].lower())) 


tokens = []   
for i in range(len(array)):
  tokens.append(word_tokenize(array[i]))

final_tokens=[]
sw = stopwords.words("spanish")
for i in range(len(tokens)):
  final_tokens.append([w for w in tokens[i] if not w.lower() in sw])

dic = final_tokens

print(dic[0])

D1=input()
mensaje=D1
D1=re.sub('[^A-Za-z\u00C0-\u017F]+',' ',D1)
D1=D1.lower()
txt_tokens = word_tokenize(D1)
vtxt = [word for word in txt_tokens if not word in stopwords.words("spanish")]


aux=[]
for i in range(len(dic)):
  aux.append(textdistance.jaccard(dic[i],vtxt))


jackard_matrix=np.matrix(aux,dtype=float) 

print(jackard_matrix)



aux1=[]
for i in range(len(dic)):
  aux1.append(textdistance.cosine(dic[i],vtxt))


coseno_matrix=np.matrix(aux1,dtype=float) 

print(coseno_matrix)


aux2=[]
for i in range(len(dic)):
  aux2.append(textdistance.tversky(dic[i],vtxt))


tversky_matrix=np.matrix(aux2,dtype=float) 

print(tversky_matrix)



aux3=[]
for i in range(len(dic)):
  aux3.append(textdistance.tanimoto.normalized_distance(dic[i],vtxt))


tanimoto_matrix=np.matrix(aux3,dtype=float) 

print(tanimoto_matrix)