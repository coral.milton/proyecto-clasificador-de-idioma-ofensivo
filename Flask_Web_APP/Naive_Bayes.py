import re  
import nltk
import numpy as np
nltk.download('stopwords')
nltk.download('punkt')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import pickle
############################################################# Pruebas del Modelo

loaded_model = pickle.load(open("modelo1", 'rb'))  # abrir modelo 
loaded_tf_idf=pickle.load(open("tf_idf", 'rb'))  # Con esto abres el modelo las 2 lineas 


D1="Texto ejemplo"
D1=re.sub('[^A-Za-z\u00C0-\u017F]+',' ',D1)
D1=D1.lower()

txt_tokens = word_tokenize(D1)
vtxt = [word for word in txt_tokens if not word in stopwords.words("spanish")]

diccionario1 = vtxt
 


X_test= loaded_tf_idf.transform([" ".join(diccionario1)]).toarray()
print(diccionario1)
pred=loaded_model.predict(X_test)
probabilidades=loaded_model.predict_proba(X_test)

if pred==0:
  print("Insultos Sexuales (clase 0)")
  print(probabilidades)
if pred==1:
  print("Insultos Discriminatorios (clase 1)")  
  print(probabilidades)
if pred==2:
  print("Insultos Xenofobicos (clase 2)")
  print(probabilidades)
if pred==3:
  print("Insultos por casta (clase 3)")
  print(probabilidades)
if pred==4:
  print("Insultos internacionales (clase 4)")
  print(probabilidades)
if pred==5:
  print("No es un insulto")
  print(probabilidades)